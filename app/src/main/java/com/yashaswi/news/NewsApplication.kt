package com.yashaswi.news

import android.app.Application
import com.facebook.stetho.Stetho
import com.yashaswi.news.ui.di.applicationModule
import org.koin.android.ext.koin.androidContext
import org.koin.core.context.startKoin


class NewsApplication : Application() {
    override fun onCreate() {
        super.onCreate()
        appplicationInstance = this
        Stetho.initializeWithDefaults(this)
        initKoin()

    }

    /**
     * initialises koin
     */
    private fun initKoin() {
        startKoin {
            androidContext(applicationContext)
            modules(listOf(applicationModule))
        }
    }

    companion object {
        var appplicationInstance: NewsApplication? = null
    }
}