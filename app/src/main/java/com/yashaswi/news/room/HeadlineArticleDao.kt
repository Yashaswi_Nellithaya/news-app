package com.yashaswi.news.room

import androidx.room.Dao
import androidx.room.Insert
import androidx.room.OnConflictStrategy
import androidx.room.Query
import com.yashaswi.news.dtos.HeadlineArticleDTO
import io.reactivex.Completable
import io.reactivex.Flowable

/**
 * Created by Yashaswi N P on 21/5/20.
 */

@Dao
interface HeadlineArticleDao {

    @Query("SELECT * from headline_article")
    fun getAllHeadlines(): Flowable<List<HeadlineArticleDTO>>

    @Insert(onConflict = OnConflictStrategy.REPLACE)
    fun insertArticles(articles: List<HeadlineArticleDTO>): Completable

    @Insert(onConflict = OnConflictStrategy.REPLACE)
    fun insertArticle(articles: HeadlineArticleDTO): Completable

}