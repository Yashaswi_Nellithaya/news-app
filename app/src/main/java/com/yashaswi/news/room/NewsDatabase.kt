package com.yashaswi.news.room

import android.content.Context
import androidx.room.Database
import androidx.room.Room
import androidx.room.RoomDatabase
import com.yashaswi.news.dtos.HeadlineArticleDTO

/**
 * Created by Yashaswi N P on 21/5/20.
 */

@Database(entities = arrayOf(HeadlineArticleDTO::class), version = 4, exportSchema = false)
abstract class NewsRoomDatabase : RoomDatabase() {

    abstract fun headlineArticleDao(): HeadlineArticleDao

    companion object {
        // Singleton prevents multiple instances of database opening at the
        // same time.
        @Volatile
        private var INSTANCE: NewsRoomDatabase? = null

        fun getDatabase(context: Context): NewsRoomDatabase {
            val tempInstance = INSTANCE
            if (tempInstance != null) {
                return tempInstance
            }
            synchronized(this) {
                val instance = Room.databaseBuilder(
                    context.applicationContext,
                    NewsRoomDatabase::class.java,
                    "news_database"
                ).build()
                INSTANCE = instance
                return instance
            }
        }
    }
}