package com.yashaswi.news.repositories

import android.annotation.SuppressLint
import android.util.Log
import androidx.lifecycle.MutableLiveData
import com.yashaswi.news.dtos.HeadlineArticleDTO
import com.yashaswi.news.network.ApiComponent
import com.yashaswi.news.room.HeadlineArticleDao
import com.yashaswi.news.utils.AppConstants
import com.yashaswi.news.utils.PreferenceUtils
import io.reactivex.android.schedulers.AndroidSchedulers
import io.reactivex.disposables.Disposable
import io.reactivex.schedulers.Schedulers

/**
 * Created by Yashaswi N P on 21/5/20.
 *
 * This class is responsible for fetching the data either from remote or local database
 */
class NewsRepository(private val headlinesDao: HeadlineArticleDao) {
    private val TAG = this.javaClass.simpleName
    private var disposable: Disposable? = null
    private val apiComponent = ApiComponent
    private var isDataAdded = false
    private val prefUtils = PreferenceUtils

    init {
        // meanwhile fetches the data and updates
        fetchHeadlinesFromApi()
    }

    /**
     * api call to fetch the headline news
     */
    fun fetchHeadlineNews(): MutableLiveData<List<HeadlineArticleDTO>> {
        return if (prefUtils.isFirstLaunch()) {
            prefUtils.setIsFirstLaunch(false)
            fetchHeadlinesFromApi()
        } else {
            fetchHeadlinesFromRoom()
        }
    }


    /**
     * fetched data from local Room db
     */
    private fun fetchHeadlinesFromRoom(): MutableLiveData<List<HeadlineArticleDTO>> {
        val articlesData = MutableLiveData<List<HeadlineArticleDTO>>()
        disposable = headlinesDao.getAllHeadlines()
            .subscribeOn(Schedulers.io())
            .observeOn(AndroidSchedulers.mainThread())
            .subscribe({
                articlesData.value = it
            }, {
                Log.e("NewsRepository", it.message.toString())
                disposable?.dispose()
            })
        return articlesData
    }

    /**
     * fetches data from API
     */
    private fun fetchHeadlinesFromApi(): MutableLiveData<List<HeadlineArticleDTO>> {
        val headlineNews = MutableLiveData<List<HeadlineArticleDTO>>()
        disposable = apiComponent.getApiInterface()
            .getNewsHeadlines("in", AppConstants.API_KEY, 35)
            .subscribeOn(Schedulers.io())
            .observeOn(AndroidSchedulers.mainThread())
            .subscribe({
                if (it.isSuccessful) {
                    headlineNews.value = it.body()?.getArticles()
                    it.body()?.getArticles()?.let { it1 -> addHeadlineArticlesToRoom(it1) }
                }
            }, {
                headlineNews.value = null
                Log.e(TAG, it.message + it.stackTrace)
                //  disposable?.dispose()
            }, {
                disposable?.dispose()
            })
        return headlineNews
    }

    /**
     * adds data to the room db
     */
    @SuppressLint("CheckResult")
    private fun addHeadlineArticlesToRoom(articles: List<HeadlineArticleDTO>) {
        headlinesDao.insertArticles(articles)
            .subscribeOn(Schedulers.io())
            .observeOn(Schedulers.newThread())
            .subscribe {
                isDataAdded = true
            }
    }
}