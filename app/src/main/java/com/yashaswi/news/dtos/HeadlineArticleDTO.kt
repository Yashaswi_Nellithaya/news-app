package com.yashaswi.news.dtos

import androidx.annotation.NonNull
import androidx.room.Entity
import androidx.room.PrimaryKey
import com.google.gson.annotations.SerializedName
import java.io.Serializable

/**
 * Created by Yashaswi N P on 24/5/20.
 */

@Entity(tableName = "headline_article")
class HeadlineArticleDTO() : Serializable {

    @PrimaryKey
    @NonNull
    @SerializedName("title")
    private var title: String = "default"

    @SerializedName("description")
    private var description: String? = null

    @SerializedName("publishedAt")
    private var publishedDate: String? = null

    @SerializedName("urlToImage")
    private var imageUrl: String? = null

    @SerializedName("author")
    private var author: String? = null

    @SerializedName("url")
    private var url: String? = null


    fun getTitle(): String? {
        return title
    }

    fun setTitle(title: String?) {
        this.title = title.toString()
    }

    fun getAuthor(): String? {
        return author
    }

    fun setAuthor(authors: String?) {
        this.author = authors
    }

    fun getPublishedDate(): String? {
        return publishedDate
    }

    fun setPublishedDate(publishedDate: String?) {
        this.publishedDate = publishedDate
    }

    fun getImageUrl(): String? {
        return imageUrl
    }

    fun setImageUrl(url: String?) {
        this.imageUrl = url
    }

    fun getDescription(): String? {
        return description
    }

    fun setDescription(description: String?) {
        this.description = description
    }


    fun getUrl(): String? {
        return url
    }

    fun setUrl(url: String?) {
        this.url = url.toString()
    }
}
