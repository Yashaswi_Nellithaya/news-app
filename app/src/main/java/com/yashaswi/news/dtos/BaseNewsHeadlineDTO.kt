package com.yashaswi.news.dtos

import com.google.gson.annotations.SerializedName
import java.io.Serializable

/**
 * Created by Yashaswi N P on 21/5/20.
 */
class BaseNewsHeadlineDTO() : Serializable {
    @SerializedName("status")
    private var status: String = ""

    @SerializedName("totalResults")
    private var totalResults: Int = 0

    @SerializedName("articles")
    private var articles: List<HeadlineArticleDTO> = ArrayList()


    fun getArticles(): List<HeadlineArticleDTO> {
        return articles
    }

    fun setArticles(articles: List<HeadlineArticleDTO>) {
        this.articles = articles
    }
}