package com.yashaswi.news.ui.activities

import android.content.Intent
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.os.Handler
import com.yashaswi.news.R
import com.yashaswi.news.utils.SystemUtils

class SplashActivity : AppCompatActivity() {

    private val systemUtils = SystemUtils

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        Handler().postDelayed({
            startActivity(Intent(this, NewsListingActivity::class.java))
            finish()
        }, 2000)
        systemUtils.hideBottomNavigationBar(this)
        systemUtils.setCustomNavigationBarColor(R.color.transparentBlack,this)
    }
}
