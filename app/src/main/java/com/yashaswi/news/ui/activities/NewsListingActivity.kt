package com.yashaswi.news.ui.activities

import android.content.Intent
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import androidx.lifecycle.Observer
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import com.yashaswi.news.R
import com.yashaswi.news.dtos.HeadlineArticleDTO
import com.yashaswi.news.ui.adapters.NewsListAdapter
import com.yashaswi.news.ui.listeners.OnItemClickListener
import com.yashaswi.news.utils.AppConstants
import com.yashaswi.news.utils.SystemUtils
import com.yashaswi.news.viewmodels.NewsViewModel
import kotlinx.android.synthetic.main.layout_home_listing.*
import org.koin.android.viewmodel.ext.android.viewModel

/**
 * Created by Yashaswi N P on 20/5/20.
 */
class NewsListingActivity : AppCompatActivity() {
    private val headLinesViewModel by viewModel<NewsViewModel>()
    private lateinit var newsAdapter: NewsListAdapter

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.layout_home_listing)
        doInitialSetUp()

    }

    /**
     * initialises the basic views and data
     */
    private fun doInitialSetUp() {
        SystemUtils.setCustomNavigationBarColor(R.color.transparentBlack, this)

        //set up recyclerview
        newsAdapter = NewsListAdapter(this, onNewsItemClickListener)
        newsRV.layoutManager = LinearLayoutManager(this, RecyclerView.VERTICAL, false)
        newsRV.adapter = newsAdapter

        //  assignViewModels()
        subscribeViewModels()
    }


    /**
     * all the api calls goes here
     */
    private fun subscribeViewModels() {
        getHeadlineNews()
    }

    /**
     *  to get the  news headlines
     */
    private fun getHeadlineNews() {
        //set up recyclerview
        headLinesViewModel.getHeadlineNews().observe(this, Observer { articles ->
            articles?.let {
                newsAdapter.setDataAndRefresh(it)
            }
        })
    }

    //TODO change the param to model
    private val onNewsItemClickListener: OnItemClickListener<HeadlineArticleDTO> =
        object : OnItemClickListener<HeadlineArticleDTO> {
            override fun onItemClick(item: HeadlineArticleDTO) {
                val detailIntent = Intent(this@NewsListingActivity, NewsDetailActivity::class.java)
                detailIntent.putExtra(AppConstants.DETAIL_DATA, item)
                startActivity(detailIntent)
            }
        }
}
