package com.yashaswi.news.ui.di

import com.yashaswi.news.network.ApiComponent
import com.yashaswi.news.room.NewsRoomDatabase
import com.yashaswi.news.utils.PreferenceUtils
import com.yashaswi.news.utils.SystemUtils
import com.yashaswi.news.viewmodels.NewsViewModel
import org.koin.android.viewmodel.dsl.viewModel
import org.koin.dsl.module

/**
 * Created by Yashaswi N P on 21/5/20.
 * contains all the koin modules
 */
val applicationModule = module {
    single { ApiComponent }
    single { NewsRoomDatabase }
    single { PreferenceUtils }
    single { SystemUtils }
    viewModel {
        NewsViewModel(get())
    }
}