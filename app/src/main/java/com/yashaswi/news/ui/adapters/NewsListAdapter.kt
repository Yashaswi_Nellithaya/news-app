package com.yashaswi.news.ui.adapters

import android.content.Context
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.ImageView
import android.widget.TextView
import androidx.recyclerview.widget.RecyclerView
import com.bumptech.glide.Glide
import com.yashaswi.news.R
import com.yashaswi.news.dtos.HeadlineArticleDTO
import com.yashaswi.news.ui.listeners.OnItemClickListener
import com.yashaswi.news.utils.TimeUtils
import kotlinx.android.synthetic.main.layout_news_item.view.*

class NewsListAdapter(context: Context, clickListener: OnItemClickListener<HeadlineArticleDTO>) :
    RecyclerView.Adapter<NewsListAdapter.NewsListViewHolder>() {
    private var mContext: Context = context
    var mOnClickListener = clickListener
    var items: List<HeadlineArticleDTO> = ArrayList()

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): NewsListViewHolder {
        val view = LayoutInflater.from(mContext).inflate(R.layout.layout_news_item, parent, false)
        return NewsListViewHolder(view)
    }

    override fun getItemCount(): Int {
        return items.size
    }

    override fun onBindViewHolder(holder: NewsListViewHolder, position: Int) {
        if (items.isNotEmpty()) {
            val article = items[position]
            Glide.with(mContext)
                .load(article.getImageUrl())
                .placeholder(R.drawable.placeholder)
                .into(holder.mBannerIV)
            holder.mNewsTitleTV.text = article.getTitle()
            holder.mNewsSourceTV.text = article.getAuthor()
            holder.mNewsDateTV.text = article.getPublishedDate()?.let {
                TimeUtils.getDateInYYYYMMDDFormat(
                    it
                )
            }
        }
    }

    /**
     * to get the data from activities / fragments
     */
    fun setDataAndRefresh(articleData: List<HeadlineArticleDTO>) {
        this.items = articleData
        notifyDataSetChanged()
    }

    inner class NewsListViewHolder(itemView: View) : RecyclerView.ViewHolder(itemView),
        View.OnClickListener {
        val mBannerIV: ImageView = itemView.newsBannerIV
        val mNewsTitleTV: TextView = itemView.newsListTitleTV
        val mNewsSourceTV: TextView = itemView.newsListSourceTV
        val mNewsDateTV: TextView = itemView.newsListDateTV

        init {
            itemView.setOnClickListener(this)
        }

        override fun onClick(v: View?) {
            mOnClickListener.onItemClick(items[adapterPosition])
        }
    }

}