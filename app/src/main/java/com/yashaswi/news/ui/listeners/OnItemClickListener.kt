package com.yashaswi.news.ui.listeners

/**
 * Created by Yashaswi N P on 21/5/20.
 */

interface OnItemClickListener<T> {
    /**
     * Invokes once the item is clicked
     */
    fun onItemClick(item: T)
}