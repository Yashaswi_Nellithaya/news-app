package com.yashaswi.news.ui.activities

import android.annotation.SuppressLint
import android.os.Bundle
import android.text.TextUtils
import android.webkit.WebViewClient
import androidx.appcompat.app.AppCompatActivity
import com.yashaswi.news.R
import com.yashaswi.news.utils.AppConstants
import kotlinx.android.synthetic.main.activity_webview.*


class WebviewActivity : AppCompatActivity() {
    private var webUrl: String? = null

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_webview)
        initView()
    }

    /**
     * initialises the views
     */
    @SuppressLint("SetJavaScriptEnabled")
    private fun initView() {
        if (extraData()) {
            newsMoreWV.webViewClient = WebViewClient()
            newsMoreWV.settings.javaScriptEnabled = true
            newsMoreWV.settings.domStorageEnabled = true
            newsMoreWV.loadUrl(webUrl)
        } else finish()
    }

    private fun extraData(): Boolean {
        webUrl = intent.getStringExtra(AppConstants.WEB_URL)
        return !TextUtils.isEmpty(webUrl)
    }
}
