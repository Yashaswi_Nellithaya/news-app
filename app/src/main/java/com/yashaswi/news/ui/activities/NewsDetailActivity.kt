package com.yashaswi.news.ui.activities

import android.content.Intent
import android.os.Bundle
import androidx.appcompat.app.AppCompatActivity
import com.bumptech.glide.Glide
import com.yashaswi.news.R
import com.yashaswi.news.dtos.HeadlineArticleDTO
import com.yashaswi.news.ui.listeners.OnSwipeTouchListener
import com.yashaswi.news.utils.AppConstants
import com.yashaswi.news.utils.SystemUtils
import com.yashaswi.news.utils.TimeUtils
import kotlinx.android.synthetic.main.layout_news_detail.*


/**
 * Created by Yashaswi N P on 20/5/20.
 */
class NewsDetailActivity : AppCompatActivity() {
    private var detailedHeadline: HeadlineArticleDTO? = null
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.layout_news_detail)
        initViews()
    }

    /**
     * initialises the views
     */
    private fun initViews() {
        SystemUtils.setCustomNavigationBarColor(R.color.transparentBlack, this)

        if (extractData()) {
            Glide.with(this)
                .load(detailedHeadline?.getImageUrl())
                .placeholder(R.drawable.placeholder)
                .into(newsDetailIV)
            newsDetailTitleTV.text = detailedHeadline?.getTitle()
            newsDetailSourceTV.text = detailedHeadline?.getAuthor()
            newsDetailDescTV.text = detailedHeadline?.getDescription()
            newsDetailDateTV.text = detailedHeadline?.getPublishedDate()?.let {
                TimeUtils.getDateInYYYYMMDDFormat(
                    it
                )
            }
            newsDetailBackIB.setOnClickListener { onBackPressed() }
            newsDetailMoreIV.setOnClickListener {
                openDetailWebView()
            }
            newsDetailMoreIV.setOnTouchListener(onSwipeTouchListener)
        }

    }

    /**
     * swipe listener
     */
    private val onSwipeTouchListener: OnSwipeTouchListener =
        object : OnSwipeTouchListener(baseContext) {
            override fun onSwipeTop() {
                openDetailWebView()
            }
        }

    /**
     * intent to webview activity
     */
    private fun openDetailWebView() {
        val intent = Intent(this@NewsDetailActivity, WebviewActivity::class.java)
        intent.putExtra(AppConstants.WEB_URL, detailedHeadline?.getUrl())
        startActivity(intent)
    }

    /**
     * extracts data from intents
     */
    private fun extractData(): Boolean {
        val intentData: Any? = intent.getSerializableExtra(AppConstants.DETAIL_DATA) ?: null

        if (intentData is HeadlineArticleDTO)
            detailedHeadline = intentData

        return null != detailedHeadline
    }
}