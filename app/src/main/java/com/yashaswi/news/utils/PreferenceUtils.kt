package com.yashaswi.news.utils

import android.app.Activity
import android.content.SharedPreferences
import com.yashaswi.news.NewsApplication

/**
 * Created by Yashaswi N P on 24/5/20.
 * Shared preference class responsible to store small set of data locally
 */

object PreferenceUtils {
    private const val FIRST_APP_LAUNCH = "firstLaunch"

    private fun editPreference(editorMethod: (SharedPreferences.Editor) -> Unit) {
        val editor = NewsApplication.appplicationInstance?.getSharedPreferences(
            NewsApplication.appplicationInstance?.packageName,
            Activity.MODE_PRIVATE
        )?.edit()
        editor?.run {
            editorMethod(editor)
            editor.apply()
        }
    }

    private fun <T> getPreference(getMethod: (SharedPreferences) -> T, defaultValue: T): T {
        val sharedPreferences = NewsApplication.appplicationInstance?.getSharedPreferences(
            NewsApplication.appplicationInstance?.packageName,
            Activity.MODE_PRIVATE
        )
        return sharedPreferences?.run(getMethod) ?: defaultValue
    }

    private fun putBoolean(key: String, value: Boolean) {
        editPreference { it.putBoolean(key, value) }
    }

    private fun getBoolean(key: String, defValue: Boolean): Boolean {
        return getPreference({ it.getBoolean(key, defValue) }, defValue)
    }


    /**
     * sets is the app is launched for the first time or not
     */
    fun setIsFirstLaunch(isFirst: Boolean) {
        putBoolean(FIRST_APP_LAUNCH, isFirst)
    }

    /**
     * gets the app launch status
     */
    fun isFirstLaunch(): Boolean {
        return getBoolean(FIRST_APP_LAUNCH, true)
    }


}