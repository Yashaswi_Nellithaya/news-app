@file:Suppress("RECEIVER_NULLABILITY_MISMATCH_BASED_ON_JAVA_ANNOTATIONS")

package com.yashaswi.news.utils

import android.annotation.SuppressLint
import java.text.SimpleDateFormat

/**
 * Created by Yashaswi N P on 21/5/20.
 */

object TimeUtils {
    @SuppressLint("SimpleDateFormat")
    fun getDateInYYYYMMDDFormat(stringDate: String): String {
        val parser = SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ss")
        val formatter = SimpleDateFormat("yyyy.MM.dd")
        val output = formatter.format(parser.parse(stringDate))
        return output.toString()
    }
}