package com.yashaswi.news.utils

import android.app.Activity
import android.os.Build
import android.view.View
import androidx.core.content.ContextCompat

/**
 * Created by Yashaswi N P on 24/5/20.
 */

object SystemUtils {

    /**
     * changes the bottom navigation bar background to provided one
     */
    fun setCustomNavigationBarColor(color: Int, activity: Activity) {
        if (Build.VERSION.SDK_INT >= 21) {
            activity.window.navigationBarColor = ContextCompat.getColor(
                activity,
                color
            )
        }
    }

    /**
     * hides the bottom navigation bar
     */
    fun hideBottomNavigationBar(activity: Activity) {
        if (Build.VERSION.SDK_INT >= 21) {
            activity.window.decorView.apply {
                systemUiVisibility =
                    View.SYSTEM_UI_FLAG_HIDE_NAVIGATION or View.SYSTEM_UI_FLAG_FULLSCREEN
            }
        }
    }
}