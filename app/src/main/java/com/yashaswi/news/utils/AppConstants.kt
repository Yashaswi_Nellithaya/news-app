package com.yashaswi.news.utils

/**
 * Created by Yashaswi N P on 21/5/20.
 */

object AppConstants {

    /* network related constants */
    const val REQUEST_READ_TIME_OUT_IN_SECONDS: Long = 30
    const val REQUEST_WRITE_TIME_OUT_IN_MINUTES: Long = 1
    const val BASE_URL = "https://newsapi.org/"
    const val API_KEY = "2c42b3053fcb41d9a9e4509525f0c4e8"
    const val GET_HEADLINES = "v2/top-headlines"


    const val DETAIL_DATA: String = "DETAIL_DATA"
    const val WEB_URL: String = "WEB_URL"

}