package com.yashaswi.news.network

import com.yashaswi.news.dtos.BaseNewsHeadlineDTO
import com.yashaswi.news.utils.AppConstants
import io.reactivex.Observable
import retrofit2.Response
import retrofit2.http.GET
import retrofit2.http.Query

/**
 * Created by Yashaswi N P on 21/5/20.
 */

interface ApiService {

    // api endpoint to to get the news headlines
    @GET(AppConstants.GET_HEADLINES)
    fun getNewsHeadlines(
        @Query("country") country: String,
        @Query("apiKey") apiKey: String,
        @Query("pageSize") pageSize: Int
    ): Observable<Response<BaseNewsHeadlineDTO>>
}