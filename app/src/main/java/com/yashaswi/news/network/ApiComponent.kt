package com.yashaswi.news.network

import com.yashaswi.news.utils.AppConstants.BASE_URL
import com.yashaswi.news.utils.AppConstants.REQUEST_READ_TIME_OUT_IN_SECONDS
import com.yashaswi.news.utils.AppConstants.REQUEST_WRITE_TIME_OUT_IN_MINUTES
import okhttp3.OkHttpClient
import retrofit2.Retrofit
import retrofit2.adapter.rxjava2.RxJava2CallAdapterFactory
import retrofit2.converter.gson.GsonConverterFactory
import java.util.concurrent.TimeUnit

/**
 * Created by Yashaswi N P on 21/5/20.
 */
object ApiComponent {

    /**
     * Configure Network Request client
     */
    fun getApiInterface(): ApiService {
        val httpClientBuilder = OkHttpClient.Builder()
            .readTimeout(REQUEST_READ_TIME_OUT_IN_SECONDS, TimeUnit.SECONDS)
            .writeTimeout(REQUEST_WRITE_TIME_OUT_IN_MINUTES, TimeUnit.MINUTES)

        val client = httpClientBuilder.build()
        return Retrofit.Builder()
            .client(client)
            .addCallAdapterFactory(RxJava2CallAdapterFactory.create())
            .addConverterFactory(GsonConverterFactory.create())
            .baseUrl(BASE_URL)
            .build()
            .create(ApiService::class.java)


    }

}
