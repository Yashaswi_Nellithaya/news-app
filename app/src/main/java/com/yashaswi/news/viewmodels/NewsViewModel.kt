package com.yashaswi.news.viewmodels

import android.content.Context
import androidx.lifecycle.LiveData
import androidx.lifecycle.ViewModel
import com.yashaswi.news.dtos.HeadlineArticleDTO
import com.yashaswi.news.repositories.NewsRepository
import com.yashaswi.news.room.HeadlineArticleDao
import com.yashaswi.news.room.NewsRoomDatabase

/**
 * Created by Yashaswi N P on 21/5/20.
 */

class NewsViewModel(context: Context) : ViewModel() {
    private var headlineNewsRepository: NewsRepository
    private var newsDatabase = NewsRoomDatabase
    private var headlinesDao: HeadlineArticleDao

    init {
        headlinesDao = newsDatabase.getDatabase(context.applicationContext).headlineArticleDao()
        headlineNewsRepository = NewsRepository(headlinesDao)
    }

    /**
     * returns the headline news
     */
    fun getHeadlineNews(): LiveData<List<HeadlineArticleDTO>> {
        return headlineNewsRepository.fetchHeadlineNews()

    }
}