package com.yashaswi.news.ui.activities


import androidx.test.espresso.Espresso.onView
import androidx.test.espresso.action.ViewActions.click
import androidx.test.espresso.action.ViewActions.swipeUp
import androidx.test.espresso.assertion.ViewAssertions.matches
import androidx.test.espresso.matcher.ViewMatchers.*
import androidx.test.ext.junit.runners.AndroidJUnit4
import androidx.test.filters.SmallTest
import androidx.test.rule.ActivityTestRule
import com.yashaswi.news.R
import com.yashaswi.news.utils.TimeUtils
import org.hamcrest.CoreMatchers.allOf
import org.junit.Rule
import org.junit.Test
import org.junit.runner.RunWith

/**
 * Created by Yashaswi N P on 23/5/20.
 */
@RunWith(AndroidJUnit4::class)
@SmallTest
class NewsDetailActivityTest {

    @get : Rule
    var mActivityRule: ActivityTestRule<NewsDetailActivity?>? =
        ActivityTestRule(NewsDetailActivity::class.java)

    @Test
    fun ensureTextsChangesWorks() {

        onView(withId(R.id.newsDetailTitleTV)).check(matches(isDisplayed()))
        onView(
            allOf(
                withId(R.id.newsDetailTitleTV),
                withText("Donald Trump press secretary inadvertently reveals president's bank details")
            )
        )
        onView(
            allOf(
                withId(R.id.newsDetailDateTV),
                withText(TimeUtils.getDateInYYYYMMDDFormat("2020-05-23T16:24:58Z"))
            )
        )
        onView(allOf(withId(R.id.newsDetailSourceTV), withText("CCV")))
        onView(
            allOf(
                withId(R.id.newsDetailDescTV),
                withText("Efforts to highlight Donald Trump’s largesse during his time in office have backfired after his press secretary appeared to display the US president’s personal bank details to the world.")
            )
        )
        onView(withId(R.id.newsDetailBackIB)).check(matches(isClickable()))
            .perform(click())

        onView(withId(R.id.newsDetailMoreIV)).perform(swipeUp())
        onView(withId(R.id.newsDetailMoreIV)).check(matches(isClickable()))
            .perform(click())
    }


}