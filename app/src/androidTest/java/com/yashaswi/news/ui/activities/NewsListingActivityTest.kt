package com.yashaswi.news.ui.activities


import androidx.test.ext.junit.runners.AndroidJUnit4
import androidx.test.filters.SmallTest
import androidx.test.rule.ActivityTestRule
import org.junit.Rule
import org.junit.runner.RunWith

/**
 * Created by Yashaswi N P on 23/5/20.
 */
@RunWith(AndroidJUnit4::class)
@SmallTest
class NewsListingActivityTest{

    @get : Rule
    var mActivityRule: ActivityTestRule<NewsListingActivity?>? = ActivityTestRule(NewsListingActivity::class.java)

}