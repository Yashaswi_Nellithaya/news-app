package com.yashaswi.news.room

import androidx.room.Room
import androidx.test.ext.junit.runners.AndroidJUnit4
import androidx.test.filters.SmallTest
import androidx.test.platform.app.InstrumentationRegistry
import com.yashaswi.news.dtos.HeadlineArticleDTO
import io.reactivex.disposables.Disposable
import org.junit.After
import org.junit.Assert
import org.junit.Before
import org.junit.Test
import org.junit.runner.RunWith


/**
 * Created by Yashaswi N P on 23/5/20.
 */

@RunWith(AndroidJUnit4::class)
@SmallTest
class HeadlineArticleDaoTest {
    private var newsRooomDb: NewsRoomDatabase? = null
    private var disposal : Disposable? = null

    @Before
    fun init() {
        newsRooomDb = Room.inMemoryDatabaseBuilder(
            InstrumentationRegistry.getInstrumentation().context,
            NewsRoomDatabase::class.java
        ).build()
    }

    @After
    fun unInit() {
        newsRooomDb?.close()
        disposal?.dispose()
    }

    @Test
    fun testLoadHeadlineArticles() {
        val entity = HeadlineArticleDTO()
        entity.setTitle("test")
        entity.setDescription("test")
        entity.setPublishedDate("2010.12.10")
        entity.setAuthor("test")
        entity.setImageUrl("test")
        newsRooomDb?.headlineArticleDao()?.insertArticle(entity)
        disposal = newsRooomDb?.headlineArticleDao()?.getAllHeadlines()
            ?.subscribe {
                Assert.assertNotNull(it)
            }
    }
}