# README #

News app

* Simple news app which loads the top headlines daily
* Works offline too
* Version - 1


### Key feature of the app ###
* Written in Kotlin
* MVVM architechture
* Koin for dependency injection
* Retrofit with Rxjava for network
* Room database for offline storage
* Unit testing with Junit and Espresso

External libraries used :
* Glide for image loading
* Retrofit for network
* Facebook Stetho for database data inspect etc

Screens :
* Splash screen
* News headlines list screen
* News detail screen with swipe up gesture
* Webview screen to read entire news
